#pragma once
#include "SingleList.h"


template<typename T=int>
class DoubleList :public SingleList<T>
{

	typedef SingleList<T>::pTNode pTNode;
	typedef SingleList<T>::TNode  TNode;


public:
	DoubleList();
	~DoubleList();

	virtual void add(int elem);
	virtual void append(int elem);
	virtual bool insert(int elem, int pos);
	virtual bool remove(int elem);



};

