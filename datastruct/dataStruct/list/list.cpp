﻿// list.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include "SingleList.cpp"
#include "SingleLoopList.cpp"
#include "DoubleList.cpp"

using namespace std;
int main()
{
	SingleList<>* ls = new SingleList<>();

	ls->append(1);
	ls->append(2);
	ls->add(10);
	ls->add(20);
	ls->insert(3, 1);
	ls->remove(1);

	cout << "singleList-----------"; 
	cout << "nodeCount:" << ls->getNodeCount()<<endl;
	ls->travel(); cout << endl;
	ls->clear();
	delete ls;


	ls = new SingleLoopList<>();
	ls->append(1);
	ls->append(2);
	ls->add(10);
	ls->add(20);
	ls->insert(3, 1);
	ls->remove(1);

	cout << "singleLoopList--------" ;
	cout << "nodeCount:" << ls->getNodeCount() << endl;
	ls->travel(); cout << endl;
	ls->clear();
	delete ls;

	ls = new DoubleList<>();
	ls->append(1);
	ls->append(2);
	ls->add(10);
	ls->add(20);
	ls->insert(3, 1);
	ls->remove(1);

	cout << "DoubleList----------";
	cout << "nodeCount:" << ls->getNodeCount() << endl;
	ls->travel(); cout << endl;
	ls->clear();
	delete ls;

	ls = nullptr;
	system("pause");



}

