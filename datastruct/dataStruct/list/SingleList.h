/*-----------------------------------------------
单向链表：
只有向前的指针，应用于删除和插入频繁的操作
-------------------------------------*/

#pragma once

template<typename T=int>
class SingleList
{

protected:
	template<typename _T=T>
	struct Node {
		Node(_T _data = 0, Node* _next = nullptr,Node* _pre=nullptr) {
			nElem = _data;
			pNext = _next;
			pPre =  _pre;
		}

		_T nElem;
		Node* pNext;
		Node* pPre; //后面子类需要使用
	};


public:
	//自定义类型
	typedef Node<T>  TNode;
	typedef TNode*  pTNode;
	typedef TNode&  refTNode;


	SingleList(pTNode head = nullptr,int nodeCount=0);
	virtual ~SingleList();

	bool isEmpty();
	int getNodeCount();
	void clear();

	virtual void add(int elem);
	virtual void travel();
	virtual void append(int elem);
	virtual bool insert(int elem, int pos);
	virtual bool remove(int elem);
	virtual bool sreach(int elem);


protected:
	pTNode	m_pHead;
	int		m_nSize;

	
};

