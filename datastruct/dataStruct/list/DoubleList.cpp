#include "DoubleList.h"


template<typename T>
DoubleList<T>::DoubleList() {

}
template<typename T>
DoubleList<T>::~DoubleList() {

}

template<typename T>
void DoubleList<T>::add(int elem)
{
	pTNode node = new TNode(elem);

	if (!this->m_pHead)
	{
		this->m_pHead = node;
		this->m_nSize++;
		return;
	}

	node->pNext = this->m_pHead;
	this->m_pHead->pPre = node;
	this->m_pHead = node;
	this->m_nSize++;

}

template<typename T>
void DoubleList<T>::append(int elem)
{
	pTNode node = new TNode(elem);

	if (!this->m_pHead)
	{
		this->m_pHead = node;
		this->m_nSize++;
		return;
	}

	pTNode cur = this->m_pHead;
	//find endNode
	while (cur->pNext)
		cur = cur->pNext;

	cur->pNext = node;
	node->pPre = cur;
	this->m_nSize++;

}

template<typename T>
bool DoubleList<T>::insert(int elem, int pos)
{
	if (pos > this->m_nSize)return false;

	if (pos == 0)
	{
		add(elem);
		return true;
	}

	if (pos == this->m_nSize)
	{
		append(elem);
		return true;
	}

	pTNode cur = this->m_pHead;
	int cnt = 1;
	while (cur && pos > cnt)
	{
		cur = cur->pNext;
		cnt++;
	}

	pTNode nex = cur->pNext;
	cur->pNext = new TNode(elem);
	cur->pNext->pPre = cur;
	nex->pPre = cur->pNext;
	cur->pNext->pNext = nex;
	this->m_nSize++;
	return true;

}


template<typename T>
bool DoubleList<T>::remove(int elem)
{
	if (!this->m_pHead)return false;

	//head node situation
	if (elem == this->m_pHead->nElem)
	{
		this->m_pHead = this->m_pHead->pNext;
		this->m_pHead->pPre = nullptr;
		return true;
	}

	pTNode cur = this->m_pHead->pNext;
	while (cur)
	{
		if (cur->nElem == elem)
		{
			cur->pPre->pNext = cur->pNext;
			return true;
		}
		cur = cur->pNext;
	}

	return false;
}