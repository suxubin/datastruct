#include "SingleList.h"
#include <iostream>
using namespace std;

template<typename T>
SingleList<T>::SingleList(pTNode head,int nodeCount):
	m_pHead(head),m_nSize(nodeCount)
{
}

template<typename T>
SingleList<T>::~SingleList() 
{
}


template<typename T>
void SingleList<T>::clear() {
	pTNode cur = m_pHead;

	while (!cur) {
		pTNode tempNext = cur->pNext;
		delete cur; 
		cur = tempNext;
		
	}
	cur = m_pHead = nullptr;
		
}



template<typename T>
int SingleList<T>::getNodeCount() {
	return m_nSize;
}

template<typename T>
bool SingleList<T>::isEmpty() {
	return m_pHead == nullptr;
}


//加到后面
template<typename T>
void SingleList<T>::append(int elem)
{
	if (isEmpty())
	{
		m_pHead = new TNode(elem);
		m_nSize++;
		return;
	}

	pTNode cur = m_pHead;
	while (cur->pNext)
		cur = cur->pNext;

	cur->pNext = new TNode(elem);
	m_nSize++;
}

//加到前面
template<typename T>
void SingleList<T>::add(int elem)
{
	pTNode head = new TNode(elem);
	head->pNext = m_pHead;
	m_pHead = head;
	m_nSize++;
}

template<typename T>
bool  SingleList<T>::insert(int elem, int pos)
{
	if (m_nSize < pos)return false;

	if (pos == 0)
	{
		add(elem);
		return true;
	}

	pTNode cur = m_pHead;
	int cnt = 1;
	while (cur && pos > cnt)
	{
		cnt++;
		cur = cur->pNext;
	}

	pTNode nex = cur->pNext;
	cur->pNext = new TNode(elem);
	cur->pNext->pNext = nex;
	m_nSize++;
	return true;
}

template<typename T>
bool SingleList<T>::remove(int elem)
{
	if (!m_pHead)return false;

	if (m_pHead->nElem == elem)
	{
		m_pHead = m_pHead->pNext;
		m_nSize--;
		return true;
	}

	pTNode cur = m_pHead->pNext;
	pTNode pre = m_pHead;
	while (cur)
	{
		if (cur->nElem == elem)
		{
			pre->pNext = cur->pNext;
			m_nSize--;
			return true;
		}
		pre = cur;
		cur = cur->pNext;
	}

	return false;
}

template<typename T>
bool SingleList<T>::sreach(int elem)
{
	pTNode cur = m_pHead;

	while (cur)
	{
		if (cur->nElem == elem)return true;
		cur = cur->pNext;
	}
	return false;
}


template<typename T>
void  SingleList<T>::travel()
{
	pTNode cur = m_pHead;

	while (cur)
	{
		cout << cur->nElem << ",";
		cur = cur->pNext;
	}

}
