#pragma once
#include "SingleList.h"


template<typename T=int>
class SingleLoopList :public SingleList<T>
{

	typedef SingleList<T>::pTNode PTNode;
	typedef SingleList<T>::TNode  TNode;


public:
	SingleLoopList();
	~SingleLoopList();
	

	virtual void add(int elem);
	virtual void travel();
	virtual void append(int elem);
	virtual bool insert(int elem, int pos);
	virtual bool remove(int elem);
	virtual bool sreach(int elem);


};

