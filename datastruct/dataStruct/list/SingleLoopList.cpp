#include "SingleLoopList.h"
#include<iostream>

using namespace std;

template<typename T>
SingleLoopList<T>::SingleLoopList()
{

}

template<typename T>
SingleLoopList<T>::~SingleLoopList()
{

}


template<typename T>
void SingleLoopList<T>::add(int elem)
{
	PTNode node = new TNode(elem);
	if (!this->m_pHead)
	{
		this->m_pHead = node;
		node->pNext = this->m_pHead;
		this->m_nSize++;
		return;
	}

	PTNode cur = this->m_pHead;
	while (cur->pNext != this->m_pHead)
		cur = cur->pNext;

	PTNode nex = this->m_pHead;
	this->m_pHead = node;
	this->m_pHead->pNext = nex;
	cur->pNext = this->m_pHead;
	this->m_nSize++;

}

template<typename T>
void SingleLoopList<T>::append(int elem)
{
	PTNode node = new TNode(elem);
	if (!this->m_pHead)
	{
		this->m_pHead = node;
		node->pNext = this->m_pHead;
		this->m_nSize++;
		return;
	}

	PTNode cur = this->m_pHead;
	while (cur->pNext != this->m_pHead)
		cur = cur->pNext;

	cur->pNext = node;
	cur->pNext->pNext = this->m_pHead;
	this->m_nSize++;

}


template<typename T>
bool SingleLoopList<T>::insert(int elem, int pos)
{
	PTNode node = new TNode(elem);
	if (this->m_nSize < pos)return false;

	if (pos == 0)
	{
		add(elem);
		return true;
	}

	//not nullptr situation
	PTNode cur = this->m_pHead;
	int cnt = 1;
	while (cur->pNext != this->m_pHead && pos > cnt)
	{
		cur = cur->pNext;
		cnt++;
	}

	//end node
	if (cur->pNext == this->m_pHead)
	{
		cur->pNext = node;
		cur->pNext->pNext = this->m_pHead;
		this->m_nSize++;
		return true;
	}

	PTNode nex = cur->pNext;
	cur->pNext = node;
	cur->pNext->pNext = nex;
	this->m_nSize++;
	return true;

}

template<typename T>
bool SingleLoopList<T>::remove(int elem)
{
	if (!this->m_pHead)return false;

	PTNode cur = this->m_pHead;
	PTNode pre = this->m_pHead;

	while (cur->pNext != this->m_pHead)
	{
		if (cur->nElem == elem)
		{
			//head node situation
			if (cur == this->m_pHead)
			{ //find end node
				PTNode rear = this->m_pHead;
				while (rear != this->m_pHead)
					rear = rear->pNext;
				this->m_pHead = cur->pNext;
				rear->pNext = this->m_pHead;
				return true;
			}//if
			pre->pNext = cur->pNext;
			this->m_nSize--;
			return true;
		}//if
		pre = cur;
		cur = cur->pNext;
	}//while

	//end node 
	if (cur->pNext == this->m_pHead)
	{
		pre->pNext = cur->pNext;
		return true;
	}

	return false;
}

template<typename T>
bool SingleLoopList<T>::sreach(int elem)
{
	PTNode cur = this->m_pHead;
	while (cur->pNext != this->m_pHead)
	{
		if (cur->nElem == elem)return true;
		cur = cur->pNext;
	}
	if (cur->nElem == elem)return true;
	return false;
}

template<typename T>
void SingleLoopList<T>::travel()
{
	if (!this->m_pHead)return;

	PTNode cur = this->m_pHead;
	while (cur->pNext != this->m_pHead)
	{
		cout << cur->nElem << ",";
		cur = cur->pNext;
	}cout << cur->nElem << endl;


}




