﻿// tree.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。


//使用类模板且分开.h/.cpp文件时，需要包含.cpp文件，声明和定义在一个文件时以.hpp为后缀

#include <iostream>
#include "binaryTree.cpp"   
#include "binarySearchTree.cpp"
#include "AVLTree.cpp"
#include "RBTree.cpp"

using namespace std;

int main()
{

	binaryTree<> *bt = new binaryTree<>();
	bt->insert(0);
	bt->insert(1);
	bt->insert(2);
	bt->insert(3);
	bt->insert(4);
	bt->insert(5);
	bt->insert(6);
	bt->insert(7);
	bt->insert(8);
	bt->insert(9);
	bt->remove(3); //remove
	cout << "二叉树--当前节点总数：" << bt->getNodeCount() << endl;
	cout << "广度优先遍历：";bt->breadchTravel(); 
	cout << "深度先序遍历：";bt->preTravel(); cout << endl;
	cout << "深度中序遍历：";bt->midTravel(); cout << endl;
	cout << "深度后序遍历：";bt->lastTravel(); cout << endl;
	bt->clearTree();
	delete bt; 

	//二叉搜寻树
	bt = new binarySearchTree<>();//10,5,2,7,1,4,6,8,15,12,17,11,14,16,18
	bt->insert(10);
	bt->insert(5);
	bt->insert(2);
	bt->insert(7);
	bt->insert(1);
	bt->insert(4);
	bt->insert(6);
	bt->insert(8);
	bt->insert(15);
	bt->insert(12);
	bt->insert(17);
	bt->insert(11);
	bt->insert(14);
	bt->insert(16);
	bt->insert(18);
	//bt->remove(10);//remove

	cout << bt->remove(10) << endl;

	cout << "二叉搜寻树--当前节点总数：" << bt->getNodeCount() << endl;
	cout << "广度优先遍历："; bt->breadchTravel();
	cout << "深度先序遍历："; bt->preTravel(); cout << endl;
	cout << "深度中序遍历："; bt->midTravel(); cout << endl;
	cout << "深度后序遍历："; bt->lastTravel(); cout << endl;
	bt->clearTree();
	delete bt;


	//二叉平衡树
	bt = new AVLTree<>();//10,5,2,7,1,4,6,8,15,12,17,11,14,16,18
	bt->insert(10);
	bt->insert(5);
	bt->insert(2);
	bt->insert(7);
	bt->insert(1);
	bt->insert(4);
	bt->insert(6);
	bt->insert(8);
	bt->insert(15);
	bt->insert(12);
	bt->insert(17);
	bt->insert(11);
	bt->insert(14);
	bt->insert(16);
	bt->insert(18);

	bt->remove(7);
	bt->remove(8);
	bt->remove(6);

	cout << "二叉平衡搜寻树--当前节点总数：" << bt->getNodeCount() << endl;
	cout << "广度优先遍历："; bt->breadchTravel();
	cout << "深度先序遍历："; bt->preTravel(); cout << endl;
	cout << "深度中序遍历："; bt->midTravel(); cout << endl;
	cout << "深度后序遍历："; bt->lastTravel(); cout << endl;

	bt->clearTree();
	delete bt;

	//红黑树
	bt = new RBTree<>();
	bt->insert(10);
	bt->insert(5);
	//bt->insert(12);
	bt->insert(6);
	cout << "红黑树--当前节点总数：" << bt->getNodeCount() << endl;
	cout << "广度优先遍历："; bt->breadchTravel();
	cout << "深度先序遍历："; bt->preTravel(); cout << endl;
	cout << "深度中序遍历："; bt->midTravel(); cout << endl;
	cout << "深度后序遍历："; bt->lastTravel(); cout << endl;

	bt->clearTree();

	delete bt;
	bt = nullptr;

	system("pause");

}

