/*-----------------------------二叉树--------------
百度百科：
二叉树（binary tree）是指树中节点的度不大于2的有序树；
结点的度：一个结点拥有子树的数目称为结点的度
有序树：如果树中各棵子树的次序是有先后次序，则称该树为有序树

自己理解：就是一颗从根开始到叶子结束且每个节点最多有两个子节点的树
代码实现比较简单：具备插入，删除和遍历的功能，
----------------------------------------------------*/


#pragma once
template <typename T=int>
class binaryTree
{


protected:
	enum Color{ RED,BLACK };

	template <typename _T=T>
	struct Node {
		Node(_T elem = 0, Node* left = nullptr, 
			Node* right = nullptr,Node* parent=nullptr,Color color=RED) {
			data = elem;
			pLeftChild = left;
			pRightChild = right;
			pParent = parent;

			eColor = color;

		}

		_T data;
		Node* pLeftChild;
		Node* pRightChild;
		Node* pParent;
		Color eColor;  //只针对红黑树，在子类增加数据成员显得麻烦

	};

public:
	//自定义类型
	typedef Node<T>  TNode;
	typedef TNode*  pTNode;
	typedef TNode&  refTNode;
	

public:
	binaryTree(pTNode root = nullptr);
	virtual ~binaryTree();

	int getNodeCount();

	int getTreeHeight();
	int getChildTreeHeight(pTNode curNode);

	pTNode getMostRightNode(pTNode curNode);
	pTNode getMostLeftNode(pTNode curNode);

	pTNode getUncleNode(pTNode curNode);
	pTNode getSiblingNode(pTNode curNode);
	pTNode getGranPaNode(pTNode curNode);

	bool isLeftChild(pTNode curNode);

	pTNode getRoot();
	bool  isEmpty();

	//树的种类，不严谨，仅实现了高度差一块，
	bool isAVLTree();
	bool isAVLTree(pTNode curNode);

	//遍历
	void breadchTravel();
	//void breadchTravel(pTNode root);
	void preTravel();
	void midTravel();
	void lastTravel();
	void preTravel(pTNode curNode);
	void midTravel(pTNode curNode);
	void lastTravel(pTNode curNode);

	virtual void insert(T data);
	virtual bool remove(T data);


	void clearTree();
	void clearChildTree(pTNode curNode);

private:
	bool __remove(pTNode curNode);

protected:
	pTNode  m_pRoot;
	/*pTNode	m_pEndNode;*/ 
	int		m_nNodeCount;

};

