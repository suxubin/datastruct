#include "binarySearchTree.h"
#include <iostream>

using namespace std;

template<typename T>
binarySearchTree<T>::binarySearchTree(pTNode root)
	:binaryTree<T>(root)
{}

template<typename T>
binarySearchTree<T>::~binarySearchTree() {
	//cout << __FUNCTION__ << endl;
}



template<typename T>
void binarySearchTree<T>::insert(T data) {

	if (!this->m_pRoot) {
		this->m_pRoot= new TNode(data);
		this->m_nNodeCount++;
	}

	else 
		this->m_pRoot = _insert(this->m_pRoot, data);
}

template<typename T>
typename binarySearchTree<T>::pTNode 
binarySearchTree<T>::_insert(pTNode curNode,T data) {
	if (!curNode) {
		this->m_nNodeCount++;
		return new TNode(data); //递归出口
	}

	//加到右子树,含等于的情况 
	if (curNode->data <= data) {
		curNode->pRightChild = _insert(curNode->pRightChild, data);
		curNode->pRightChild->pParent = curNode;
	}	

	//加到左子树，
	else {
		curNode->pLeftChild = _insert(curNode->pLeftChild, data);
		curNode->pLeftChild->pParent = curNode;
	}
	
	
	return curNode; 

}



template<typename T>
bool binarySearchTree<T>::remove(T data) {
	return _remove(this->m_pRoot, data) != nullptr;

}

template<typename T>
typename binarySearchTree<T>::pTNode
binarySearchTree<T>::_remove(pTNode curNode, T data) {
	if (!curNode) return nullptr;

	//搜寻右子树
	if (curNode->data < data) {
		return _remove(curNode->pRightChild, data);
	}

	//搜寻左子树
	else if (curNode->data > data) {
		return _remove(curNode->pLeftChild, data);
	}

	//找到了
	else {
		pTNode tempRight = curNode->pRightChild;
		pTNode tempLeft = curNode->pLeftChild;
		pTNode tempParent = curNode->pParent;

		//叶子节点情况---只需要调整父节点一侧指针
		if (!tempLeft && !tempRight) {
			if (!tempParent) this->m_pRoot = nullptr;

			else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = nullptr;

			else if (tempParent->pRightChild == curNode) tempParent->pRightChild = nullptr;

		}

		//单一节点的情况---调整父子两侧指针
		else if (!curNode->pLeftChild || !curNode->pRightChild) {
			pTNode leftOrRight = tempLeft ? tempLeft : tempRight;

			if (!tempParent)this->m_pRoot = leftOrRight;

			else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = leftOrRight;

			else if (tempParent->pRightChild == curNode) tempParent->pRightChild = leftOrRight;

			leftOrRight->pParent = tempParent; //调整子节点一侧

		}

		//左右节点都有的情况
		else {
			//找到左树最右节点，和要删除的节点替换数据，问题变成删除左树最右节点（前面两种情况）
			pTNode mostRightLeaf =this->getMostRightNode(tempLeft);
			curNode->data = mostRightLeaf->data;
			return _remove(mostRightLeaf, mostRightLeaf->data);

		}

		delete curNode; curNode = nullptr;
		this->m_nNodeCount--;
		return this->m_pRoot;
		

	}//else 

	return nullptr;
}