#include "binaryTree.h"
#include <math.h>
#include <iostream>
#include <queue>

using namespace std;

template<typename T>
binaryTree<T>::binaryTree(pTNode root)
	:m_pRoot(root),/*m_pEndNode(root),*/m_nNodeCount(0)
{
}

template<typename T>
binaryTree<T>::~binaryTree() {
	//cout << __FUNCTION__ << endl;
}

template<typename T>
int binaryTree<T>::getNodeCount()
{
	return m_nNodeCount;
}

template<typename T>
int binaryTree<T>::getTreeHeight() {
	return getChildTreeHeight(m_pRoot);
}

template<typename T>
int binaryTree<T>::getChildTreeHeight(pTNode curNode) {
	//高度：节点到其最深子节点的路径长度（经过的边数），递归实现
	return !curNode ? 0 :
		max(getChildTreeHeight(curNode->pLeftChild),
			getChildTreeHeight(curNode->pRightChild)) + 1;
}

template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getUncleNode(pTNode curNode) {
	return !curNode ? nullptr :getSiblingNode(curNode->pParent);
	
}

template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getSiblingNode(pTNode curNode) {
	if (!curNode)return nullptr;

	if (!curNode->pParent)return nullptr;

	if (curNode->pParent->pLeftChild == curNode)return curNode->pParent->pRightChild;

	else return curNode->pParent->pLeftChild;

}

template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getGranPaNode(pTNode curNode) {
	return !curNode ? nullptr :
		(!curNode->pParent ? nullptr : curNode->pParent->pParent);
}



template<typename T>
bool binaryTree<T>::isLeftChild(pTNode curNode)
{
	return curNode && curNode->pParent &&
		curNode == curNode->pParent->pLeftChild;

}


template<typename T>
bool binaryTree<T>::isEmpty() {
	return m_pRoot == nullptr;
}

template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getRoot() {
	return m_pRoot;
}
 
template<typename T>
void binaryTree<T>::insert(T data) {
	pTNode newNode = new TNode(data);

	//根为空的情况
	if (!m_pRoot) {
		m_pRoot = newNode;
		//m_pEndNode = m_pRoot;
		m_nNodeCount++;
		return;
	}

	//利用队列实现
	queue<pTNode>tempQue;
	tempQue.push(m_pRoot);

	while (!tempQue.empty())
	{
		pTNode cur = tempQue.front();
		tempQue.pop();

		//从左节点先加
		if (!cur->pLeftChild)
		{
			cur->pLeftChild = newNode;		//赋值左子树的空节点
			cur->pLeftChild->pParent = cur; //赋值新加节点的父节点
			m_nNodeCount++;
			return;
		}
		else tempQue.push(cur->pLeftChild);//回溯，轮询找到左子树的空节点

		//右节点
		if (!cur->pRightChild)
		{
			cur->pRightChild = newNode;
			cur->pRightChild->pParent = cur;
			m_nNodeCount++;
			return;
		}
		else tempQue.push(cur->pRightChild);

	}//while
	
	
}

template<typename T>
bool binaryTree<T>::remove(T data) {

	//查找时间复杂度O(n),实现有点复杂

	//利用队列实现遍历
	queue<pTNode>tempQue;
	tempQue.push(m_pRoot);

	while (!tempQue.empty())
	{
		pTNode cur = tempQue.front();
		tempQue.pop();
		
		if (cur->data == data)  return __remove(cur);//找到目标

		if (cur->pLeftChild) tempQue.push(cur->pLeftChild);  //搜寻左子树

		if (cur->pRightChild) tempQue.push(cur->pRightChild);//搜寻右子树

	}//while

	return false;

}

template<typename T>
bool binaryTree<T>::__remove(pTNode curNode) {
	//删除规则按照存在时间长短作为衡量标准，即根》左》右，当删除根时，优先用左代替

	pTNode tempRight	= curNode->pRightChild;
	pTNode tempLeft		= curNode->pLeftChild;
	pTNode tempParent	= curNode->pParent;

	//叶子节点情况---只需要调整父节点一侧指针
	if (!tempLeft && !tempRight) {
		if (!tempParent) m_pRoot = nullptr;

		else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = nullptr;
		
		else if (tempParent->pRightChild == curNode) tempParent->pRightChild = nullptr;

	}

	//单一节点的情况---调整父子两侧指针
	else if (!curNode->pLeftChild || !curNode->pRightChild) {
		pTNode leftOrRight= tempLeft ? tempLeft : tempRight;

		if (!tempParent) m_pRoot = leftOrRight;
			
		else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = leftOrRight;
			
		else if (tempParent->pRightChild == curNode) tempParent->pRightChild = leftOrRight;
			
		leftOrRight->pParent = tempParent; //调整子节点一侧
		
	}

	//左右节点都有的情况
	else {
		//找到左树最右的叶子节点，并将删除节点的右子树添加进来
		pTNode mostRightLeaf = getMostRightNode(tempLeft);
		mostRightLeaf->pRightChild = tempRight;
		tempRight->pParent = mostRightLeaf;

		if (!tempParent) m_pRoot = tempLeft;
			
		else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = tempLeft;
			
		else if (tempParent->pRightChild == curNode) tempParent->pRightChild = tempLeft;
			
		tempLeft->pParent = tempParent;
	}

	delete curNode; curNode = nullptr;
	m_nNodeCount--;
	return true;
	
}


template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getMostLeftNode(pTNode curNode) {
	return !curNode ? curNode:
	      (!curNode->pLeftChild ? curNode : getMostLeftNode(curNode->pLeftChild));
}

template<typename T>
typename binaryTree<T>::pTNode binaryTree<T>::getMostRightNode(pTNode curNode) {
	return !curNode ? curNode :
		  (!curNode->pRightChild ? curNode : getMostRightNode(curNode->pRightChild));
}


template<typename T>
void binaryTree<T>::breadchTravel() {
	//当树为空的时候
	if (!m_pRoot)return;
	
	//利用队列实现遍历
	queue<pTNode>tempQue;
	tempQue.push(m_pRoot);

	while (!tempQue.empty())
	{
		pTNode cur = tempQue.front();
		tempQue.pop();

		cout<<cur->data<<',';

		if (cur->pLeftChild) tempQue.push(cur->pLeftChild);  //搜寻左子树

		if (cur->pRightChild) tempQue.push(cur->pRightChild);//搜寻右子树

	}//while

	cout << endl;
}


//为了兼容红黑树，因为两者数据结构有区别
//template<typename T>
//void binaryTree<T>::breadchTravel(pTNode root) {
//	//当树为空的时候
//	if (!root)return;
//
//	//利用队列实现遍历
//	queue<pTNode>tempQue;
//	tempQue.push(root);
//
//	while (!tempQue.empty())
//	{
//		pTNode cur = tempQue.front();
//		tempQue.pop();
//
//		cout << cur->data << ',';
//
//		if (cur->pLeftChild) tempQue.push(cur->pLeftChild);  //搜寻左子树
//
//		if (cur->pRightChild) tempQue.push(cur->pRightChild);//搜寻右子树
//
//	}//while
//
//	cout << endl;
//}


template<typename T>
void binaryTree<T>::preTravel()
{
	preTravel(m_pRoot);

}

template<typename T>
void binaryTree<T>::preTravel(pTNode curNode)
{
	if (!curNode)return;

	cout << curNode->data << ',';
	preTravel(curNode->pLeftChild);
	preTravel(curNode->pRightChild);
	
}


template<typename T>
void binaryTree<T>::midTravel() {
	midTravel(m_pRoot);
}

template<typename T>
void binaryTree<T>::midTravel(pTNode curNode) {
	if (!curNode)return;

	midTravel(curNode->pLeftChild);
	cout << curNode->data << ',';
	midTravel(curNode->pRightChild);
}



template<typename T>
void binaryTree<T>::lastTravel() {
	lastTravel(m_pRoot);
}

template<typename T>
void binaryTree<T>::lastTravel(pTNode curNode) {
	if (!curNode)return;

	lastTravel(curNode->pLeftChild);
	lastTravel(curNode->pRightChild);
	cout << curNode->data << ',';
}


template<typename T>
void binaryTree<T>::clearTree() {
	clearChildTree(m_pRoot);
}

template<typename T>
void binaryTree<T>::clearChildTree(pTNode curNode) {
	if (!curNode)return;

	clearChildTree(curNode->pLeftChild);
	clearChildTree(curNode->pRightChild);

	delete curNode;
	curNode = nullptr;

}


template<typename T>
bool binaryTree<T>::isAVLTree()
{
	return isAVLTree(m_pRoot);

}

template<typename T>
bool binaryTree<T>::isAVLTree(pTNode curNode)
{
	return !curNode ? true :
		abs(getChildTreeHeight(curNode->pLeftChild) - getChildTreeHeight(curNode->pRightChild)) <= 1
		&& isAVLTree(curNode->pLeftChild)
		&& isAVLTree(curNode->pRightChild);
}




