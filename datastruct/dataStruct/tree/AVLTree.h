/*--------------------------------------------------
二叉平衡树：二叉搜寻树且左右子树高度差不超过二
----------------------------------------------------*/

#pragma once
#include "binaryTree.h"

template<typename T=int>
class AVLTree :public binaryTree<T>
{
	//自定义，不然每次都需要用this麻烦
	typedef binaryTree<T>::pTNode pTNode;
	typedef binaryTree<T>::TNode  TNode;

public:
	AVLTree(pTNode root = nullptr);
	virtual ~AVLTree();

	int getFactor();
	int getFactor(pTNode curNode);

	virtual void insert(T data);
	virtual bool remove(T data);


private:
	pTNode _insert(pTNode curNode,T data);
	pTNode _leftRotate(pTNode curNode);
	pTNode _rightRotate(pTNode curNode);
	pTNode _reBalance(pTNode curNode);
	pTNode _remove(pTNode curNode, T data);

};

