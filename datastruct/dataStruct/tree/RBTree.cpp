#include "RBTree.h"


template<typename T>
RBTree<T>::RBTree(pTNode root)
	:binaryTree<T>(root)
{}

template<typename T>
RBTree<T>::~RBTree()
{}

template<typename T>
typename RBTree<T>::Color RBTree<T>::getColor() {
	return this->m_pRoot->eColor;
}

template<typename T>
void RBTree<T>::setColor(Color color) {
	this->m_pRoot->eColor = color;
}

template<typename T>
bool RBTree<T>::isBlack(pTNode curNode) {
	//null叶子节点默认为黑节点
	return !curNode ? true :
			curNode->eColor == this->BLACK;
}

template<typename T>
typename RBTree<T>::pTNode
RBTree<T> ::setBlack(pTNode curNode) {
	if (!curNode) return curNode;

	curNode->eColor = this->BLACK;
	return curNode;
}

template<typename T>
typename RBTree<T>::pTNode
RBTree<T> ::setRed(pTNode curNode) {
	if (!curNode) return curNode;

	curNode->eColor = this->RED;
	return curNode;
}


template<typename T>
void RBTree<T>::insert(T data) {
	//根节点的情况，着色为黑
	if (!this->m_pRoot) {
		this->m_pRoot = new TNode(data,nullptr,nullptr,nullptr,this->BLACK);
		this->m_nNodeCount++;
		return;
	}

	//非根节点的情况；可能需要左右旋，故根节点会改变
	else
		this->m_pRoot = _insert(this->m_pRoot, data);

}

template<typename T>
typename RBTree<T>::pTNode
RBTree<T>::_insert(pTNode curNode, T data) {
	if (!curNode) {
		this->m_nNodeCount++;
		return new TNode(data); //递归出口

	}

	//加到右子树,含等于的情况 
	if (curNode->data <= data) {
		curNode->pRightChild = _insert(curNode->pRightChild, data);
		curNode->pRightChild->pParent = curNode;
		return _reBalance(curNode->pRightChild); //新加节点作为平衡点检查，
	}

	//加到左子树，
	else {
		curNode->pLeftChild = _insert(curNode->pLeftChild, data);
		curNode->pLeftChild->pParent = curNode;
		return _reBalance(curNode->pLeftChild);
	}

	//要求从最深失衡节点开始变换，符合要求，而且按照递归的顺序最后返回的是根节点，
	//return _reBalance(curNode); //非叶子节点,是以其父节点作为平衡基点


}


template<typename T>
typename RBTree<T>::pTNode
RBTree<T>::_reBalance(pTNode curNode) {
	/*-------------------------------------------
	四阶B树，叶子节点有四种情况
		B			B		B		B
	  R	  R	      R			 R

	增加子节点总共有12中情况
	---------------------------------------------*/

	//前面已经对根节点进行判断，到这里来肯定有parent
	pTNode parent = curNode->pParent;

	pTNode uncle = this->getSiblingNode(parent);

	//1 parent=black 四种，直接返回
	if (isBlack(parent)) {
		return curNode;
	}

	//2 parent=red uncle=nullptr or black  四种,如何区分LL/RR/LR/RL
	if (isBlack(uncle)) {
		//LL
		if (this->isLeftChild(parent) && this->isLeftChild(curNode)) {
			setRed(parent->pParent);
				curNode = _rightRotate(setBlack(parent));
		}
		
		//RR
		else if (!this->isLeftChild(parent) && !this->isLeftChild(curNode)) {
			setRed(parent->pParent);
			curNode=_leftRotate(setBlack(parent));
		}

		//LR
		else if (this->isLeftChild(parent) && !this->isLeftChild(curNode)) {
			setBlack(curNode);
			setRed(parent);
			setRed(parent->pParent);

			curNode = _leftRotate(parent);
			curNode = _rightRotate(curNode);

		}

		//RL
		else if (!this->isLeftChild(parent) && this->isLeftChild(curNode)) {
			setBlack(curNode);
			setRed(parent);
			setRed(parent->pParent);

			curNode = _rightRotate(parent);
			curNode = _leftRotate(curNode);
		}

		return curNode;
	}

	//3 parent=red uncle=red  四种;
	if (!isBlack(uncle)) {
		setBlack(parent);
		setBlack(uncle);	//染色
		return _reBalance(setRed(parent->pParent));//上溢

	}

	return nullptr;
}

template<typename T>
typename RBTree<T>::pTNode
RBTree<T>::_leftRotate(pTNode curNode) {
	if (!curNode)return nullptr;

	pTNode tempRight = curNode->pRightChild;

	curNode->pRightChild = tempRight->pLeftChild;
	if (tempRight->pLeftChild)
		tempRight->pLeftChild->pParent = curNode;

	tempRight->pLeftChild = curNode;
	curNode->pParent = tempRight;

	return tempRight;

}

template<typename T>
typename RBTree<T>::pTNode
RBTree<T>::_rightRotate(pTNode curNode) {
	if (!curNode)return nullptr;

	pTNode tempLeft = curNode->pLeftChild;

	curNode->pLeftChild = tempLeft->pRightChild;
	if (tempLeft->pRightChild)//不空则调整其父节点
		tempLeft->pRightChild->pParent = curNode;

	tempLeft->pRightChild = curNode;
	curNode->pParent = tempLeft;

	return tempLeft;  //parent一侧需要调用函数实现，比较合理

}

