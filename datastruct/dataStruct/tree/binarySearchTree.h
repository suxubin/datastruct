/*-------------------------二叉搜寻树------------------
特殊的二叉树
对于树中的每个节点X，它的左子树中所有关键字值小于X的关键字；
而它的右子树中所有关键字值大于X的关键字

知识点：模板类发生继承时，需要访问基类成员时使用this指针显式指定为成员变量/函数
模板类定义阶段：  编译器会忽略继承的模板基类，会认为访问的基类函数是非成员函数；
模板类实例化阶段：编译器已经坚持认为某个基类函数是非成员函数，也就找不到报错
------------------------------------------------------*/

#pragma once
#include "binaryTree.h"

template <typename T=int>
class binarySearchTree :public binaryTree<T>
{
	//自定义，不然每次都需要用this麻烦
	typedef binaryTree<T>::pTNode pTNode;
	typedef binaryTree<T>::TNode  TNode; 

public:
	binarySearchTree(pTNode root = nullptr);
	virtual ~binarySearchTree();

	virtual void insert(T data);
	virtual bool remove(T data);

private:
	pTNode _remove(pTNode curNode, T data);
	pTNode _insert(pTNode curNode, T data);


};

