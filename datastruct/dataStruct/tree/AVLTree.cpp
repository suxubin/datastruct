#include "AVLTree.h"

template<typename T>
AVLTree<T>::AVLTree(pTNode root)
	:binaryTree<T>(root)
{}

template<typename T>
AVLTree<T>::~AVLTree()
{}


template<typename T>
int AVLTree<T>::getFactor()
{
	return getFactor(this->m_pRoot);
}

template<typename T>
int AVLTree<T>::getFactor(pTNode curNode)
{
	//左子树减去右子树
	return this->getChildTreeHeight(curNode->pLeftChild) -
		this->getChildTreeHeight(curNode->pRightChild);
}


template<typename T>
void AVLTree<T>::insert(T data)
{

	if (!this->m_pRoot) {
		this->m_pRoot = new TNode(data);
		this->m_nNodeCount++;
	}

	else
		this->m_pRoot = _insert(this->m_pRoot, data);
}

template<typename T>
typename AVLTree<T>::pTNode
AVLTree<T>::_insert(pTNode curNode,T data) {
	if (!curNode) {
		this->m_nNodeCount++;
		return new TNode(data); //递归出口
	
	}

	//加到右子树,含等于的情况 
	if (curNode->data <= data) {
		curNode->pRightChild = _insert(curNode->pRightChild, data);
		curNode->pRightChild->pParent = curNode;
	}

	//加到左子树，
	else {
		curNode->pLeftChild = _insert(curNode->pLeftChild, data);
		curNode->pLeftChild->pParent = curNode;
	}

	//要求从最深失衡节点开始变换，符合要求，而且按照递归的顺序最后返回的是根节点，
	return _reBalance(curNode); 

}

template<typename T>
bool AVLTree<T>::remove(T data) {
	return _remove(this->m_pRoot, data) != nullptr;
}
template<typename T>
typename AVLTree<T>::pTNode
 AVLTree<T>::_remove(pTNode curNode,T data) {
	if (!curNode)return nullptr;

	//搜寻右子树
	if (curNode->data < data) {
		return _remove(curNode->pRightChild, data);
	}

	//搜寻左子树
	else if (curNode->data > data) {
		return _remove(curNode->pLeftChild, data);
	}

	//找到了，就需要修改节点指针
	else {
		pTNode tempRight = curNode->pRightChild;
		pTNode tempLeft = curNode->pLeftChild;
		pTNode tempParent = curNode->pParent;

		//叶子节点情况---只需要调整父节点一侧指针
		if (!tempLeft && !tempRight) {
			if (!tempParent) this->m_pRoot = nullptr;

			else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = nullptr;

			else if (tempParent->pRightChild == curNode) tempParent->pRightChild = nullptr;

		}

		//单一节点的情况---调整父子两侧指针
		else if (!curNode->pLeftChild || !curNode->pRightChild) {
			pTNode leftOrRight = tempLeft ? tempLeft : tempRight;

			if (!tempParent)this->m_pRoot = leftOrRight;

			else if (tempParent->pLeftChild == curNode) tempParent->pLeftChild = leftOrRight;

			else if (tempParent->pRightChild == curNode) tempParent->pRightChild = leftOrRight;

			leftOrRight->pParent = tempParent; //调整子节点一侧

		}

		//左右节点都有的情况
		else {
			//找到左树最右节点，和要删除的节点替换数据，问题变成删除左树最右节点（前面两种情况）
			pTNode mostRightLeaf = this->getMostRightNode(tempLeft);
			curNode->data = mostRightLeaf->data;
			return _remove(mostRightLeaf, mostRightLeaf->data);

		}

		//三种情况统一由这里返回
		//delete curNode; curNode = nullptr;
		this->m_nNodeCount--;
		curNode= _reBalance(tempParent);

		if (!curNode->pParent)this->m_pRoot = curNode;

		curNode->pParent = tempParent;

		return curNode;


	}//else 

	return nullptr; //找不到由这里返回
}



template<typename T>
typename AVLTree<T>::pTNode
AVLTree<T>::_reBalance(pTNode curNode) {
	int factor = getFactor(curNode); //左子树减去右子树的度

	pTNode tempParent = curNode->pParent;

	//LL情况--右旋
	if (factor > 1 && getFactor(curNode->pLeftChild) > 0) {
		curNode = _rightRotate(curNode);
		curNode->pParent = tempParent;
	}

	//LR情况（等于0的情况可能不会发生）--左旋+右旋
	else if (factor > 1 && getFactor(curNode->pLeftChild) <= 0) {
		curNode = _leftRotate(curNode->pLeftChild);
		curNode->pParent = tempParent;

		curNode = _rightRotate(curNode);
		curNode->pParent = tempParent;
	}

	//RR--左旋
	else if (factor < -1 && getFactor(curNode->pRightChild) < 0) {
		curNode = _leftRotate(curNode);
		curNode->pParent = tempParent;
	}

	//RL---右旋+左旋
	else if (factor < -1 && getFactor(curNode->pRightChild) >= 0) {
		curNode = _rightRotate(curNode->pRightChild);
		curNode->pParent = tempParent;

		curNode = _leftRotate(curNode);
		curNode->pParent = tempParent;
	}

	//隐含else即没有失衡的情况, 什么都不做
	return curNode;		//统一由一个接口返回


}

template<typename T>
typename AVLTree<T>::pTNode
AVLTree<T>::_leftRotate(pTNode curNode) {
	if (!curNode)return nullptr;

	pTNode tempRight = curNode->pRightChild;

	curNode->pRightChild=tempRight->pLeftChild;
	if (tempRight->pLeftChild)
		tempRight->pLeftChild->pParent = curNode;

	tempRight->pLeftChild = curNode;
	curNode->pParent = tempRight;

	return tempRight;
	
}

template<typename T>
typename AVLTree<T>::pTNode
AVLTree<T>::_rightRotate(pTNode curNode) {
	if (!curNode)return nullptr;

	pTNode tempLeft = curNode->pLeftChild;

	curNode->pLeftChild = tempLeft->pRightChild;
	if(tempLeft->pRightChild)//不空则调整其父节点
		tempLeft->pRightChild->pParent = curNode;

	tempLeft->pRightChild = curNode;
	curNode->pParent = tempLeft;

	return tempLeft;  //parent一侧需要调用函数实现，比较合理
	
}

