/*--------------------------------------------------
红黑树：
1节点非红即黑
2根节点是黑色的
3每个叶子节点（NULL）是黑色的
4红色节点的子节点必须是黑色的
5从根节点到叶子节点（NULL）的所有路径上黑色节点数都相同
注意：添加节点默认是红色能更好保持平衡
----------------------------------------------------*/



#pragma once
#include "binaryTree.h"

template<typename T=int>
class RBTree:public binaryTree<T>
{
protected:
	//使用父类的类型
	typedef binaryTree<T>::pTNode pTNode;
	typedef binaryTree<T>::TNode  TNode;
	typedef binaryTree<T>::Color Color;
		

public:
	RBTree(pTNode root = nullptr);
	~RBTree();

	void setColor(Color color);
	pTNode setBlack(pTNode curNode);
	pTNode setRed(pTNode curNode);
	Color getColor();

	bool isBlack(pTNode curNode);

	virtual void insert(T data);
	pTNode _insert(pTNode curNode, T data);

private:
	pTNode _reBalance(pTNode curNode);
	pTNode _leftRotate(pTNode curNode);
	pTNode _rightRotate(pTNode curNode);


	



};

