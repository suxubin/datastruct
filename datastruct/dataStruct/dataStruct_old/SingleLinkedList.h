#pragma once

class TSingleLinkedList
{
public:

	struct Node{
		Node(int elem=0,Node* node=nullptr,Node*preNode=nullptr)
		{nElem=elem;pNext=node,pPre=preNode;}
		int nElem;
		Node* pNext;
		Node* pPre;
	};

	TSingleLinkedList(Node* head=nullptr,int nSize=0);
	virtual ~TSingleLinkedList();

	bool isEmpty();
	int length();
	virtual void add(int elem);
	virtual void travel();
	virtual void append(int elem);
	virtual bool insert(int elem,int pos);
	virtual bool remove(int elem);
	virtual bool sreach(int elem);
	
protected:
	Node* m_pHead;
	int	  m_nSize;




};