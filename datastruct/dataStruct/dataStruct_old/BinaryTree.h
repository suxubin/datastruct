#pragma once

class TBinaryTree
{
public:
	struct Node
	{
		Node(int elem=0,Node* left=nullptr,Node* right=nullptr){
			nElem=elem;
			pLeftChild=left;
			pRightChild=right;
		}
		int nElem;
		Node* pLeftChild;
		Node* pRightChild;

	};

	TBinaryTree(Node* root=nullptr);
	virtual ~TBinaryTree();
	
	int getSize();
	int getTreeHeight();
	int getChildTreeHeight(Node* pChildRoot = nullptr);
	bool isEmpty();
	Node* getRoot();

	virtual void add(int elem);
	void clearTree(Node* root);

	void breadchTravel();
	void preOrder(Node* root);
	void midOrder(Node* root);
	void lastOrder(Node* root);

	bool isBalanceTree();
	bool isBalanceChildTree(Node* pChildRoot = nullptr);



protected:
	Node* m_pRoot;
	int   m_nSize;


};