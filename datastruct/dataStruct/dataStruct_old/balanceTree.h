
//平衡二叉树，应用于windows操作系统的进程管理；
//左右子树高度差小于等于1；
//按照数据的大小区分存放位置；
#pragma once
#include"BinaryTree.h"


class TBalanceTree :public TBinaryTree
{
public:
	TBalanceTree();
	virtual ~TBalanceTree();

	int getFactor();
	int getFactor(Node* pRoot);
	void insert(int elem);
	Node* insertChildTree(Node* pChildRoot, int elem);
	void deleteNode(int elem);
	Node* deleteChildTree(Node* pChildRoot, int elem);
	Node* reBalance(Node* pRoot);

	Node* findLeftLeaf(Node* pRoot);
	Node* findRightLeaf(Node* pRoot);
	
	Node* leftRotate(Node* pRoot);
	Node* rightRotate(Node* pRoot);
	/*Node* lrRotate(Node* pRoot);
	Node* rlRotate(Node* pRoot);*/


private:
	




};


