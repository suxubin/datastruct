#include <iostream>
#include "BinaryTree.h"
#include <math.h>
#include <queue>
using namespace std;


TBinaryTree::TBinaryTree(Node* root/* =nullptr */)
	:m_pRoot(root)
{
	m_nSize=0;
}

TBinaryTree::~TBinaryTree()
{
	clearTree(m_pRoot);
}

void TBinaryTree::clearTree(Node* root)
{
	if (!root) return;
	clearTree(root->pLeftChild);
	clearTree(root->pRightChild);
	delete root;
	root = nullptr;

}



int TBinaryTree::getSize()
{
	return m_nSize;
}
int TBinaryTree::getTreeHeight()
{
	return getChildTreeHeight(m_pRoot);
}
int TBinaryTree::getChildTreeHeight(Node* pRoot)
{
	return !pRoot ? 0 : max(getChildTreeHeight(pRoot->pLeftChild), 
							getChildTreeHeight(pRoot->pRightChild))+1;
}


bool TBinaryTree::isBalanceTree()
{
	return isBalanceChildTree(m_pRoot);
}
bool TBinaryTree::isBalanceChildTree(Node* pRoot)
{
	return !pRoot ? true :
		abs(getChildTreeHeight(pRoot->pLeftChild)-getChildTreeHeight(pRoot->pRightChild))<=1
		&& isBalanceChildTree(pRoot->pLeftChild)
		&& isBalanceChildTree(pRoot->pRightChild);
}


bool TBinaryTree::isEmpty()
{
	return m_nSize==0;
}

TBinaryTree::Node* TBinaryTree::getRoot()
{
	return m_pRoot;
}

void TBinaryTree::add(int elem)
{
	Node* node=new Node(elem);

	if (!m_pRoot)
	{
		m_pRoot=node;
		m_nSize++;
		return;
	}

	//use queue realize,also using myself realize que
	queue<Node*> tempQue;
	tempQue.push(m_pRoot);

	while(!tempQue.empty())
	{
		Node* cur=tempQue.front();
		tempQue.pop();

		if (!cur->pLeftChild)
		{
			cur->pLeftChild=node;
			m_nSize++;
			return;
		}else tempQue.push(cur->pLeftChild);//����

		if(!cur->pRightChild)
		{
			cur->pRightChild=node;
			m_nSize++;
			return;
		}else tempQue.push(cur->pRightChild);

	}//while

}

void TBinaryTree::breadchTravel()
{
	if(!m_pRoot)return;

	queue<Node*> tempQue;
	tempQue.push(m_pRoot);

	while (!tempQue.empty())
	{
		Node* cur=tempQue.front();
		tempQue.pop();

		cout<<cur->nElem<<",";

		if (cur->pLeftChild)tempQue.push(cur->pLeftChild);

		if(cur->pRightChild)tempQue.push(cur->pRightChild);
		
	}
	cout<<endl;

}

void TBinaryTree::preOrder(Node* root)
{
	if(!root)return;
	
	cout<<root->nElem<<",";

	preOrder(root->pLeftChild);
	preOrder(root->pRightChild);
}
void TBinaryTree::midOrder(Node* root)
{
	if(!root)return;
	midOrder(root->pLeftChild);
	cout<<root->nElem<<",";
	midOrder(root->pRightChild);

}
void TBinaryTree::lastOrder(Node* root)
{
	if(!root)return;

	lastOrder(root->pLeftChild);
	lastOrder(root->pRightChild);
	cout<<root->nElem<<",";
}
