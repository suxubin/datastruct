#include<iostream>
#include<algorithm>
#include"balanceTree.h"
#include<math.h>
using namespace std;

TBalanceTree::TBalanceTree()
{
	
}

TBalanceTree::~TBalanceTree()
{

}

int TBalanceTree::getFactor()
{
	return !m_pRoot ? 0 :
		getChildTreeHeight(m_pRoot->pLeftChild)-getChildTreeHeight(m_pRoot->pRightChild);
}
int TBalanceTree::getFactor(Node* pRoot)
{
	return !pRoot ? 0 :
		getChildTreeHeight(pRoot->pLeftChild) - getChildTreeHeight(pRoot->pRightChild);

}


void TBalanceTree::insert(int elem)
{
	m_pRoot = !m_pRoot ? new Node(elem) : insertChildTree(m_pRoot,elem);
	
}

TBalanceTree::Node* TBalanceTree::insertChildTree(Node* pRoot, int elem)
{
	if (!pRoot)return new Node(elem);

	//right
	if (pRoot->nElem < elem)
	{
		pRoot->pRightChild = insertChildTree(pRoot->pRightChild, elem);
	}
	else {
		pRoot->pLeftChild = insertChildTree(pRoot->pLeftChild, elem);
	}

	return reBalance(pRoot);
}

void TBalanceTree::deleteNode(int elem)
{
	m_pRoot = !m_pRoot ? m_pRoot : deleteChildTree(m_pRoot, elem);

}
TBalanceTree::Node* TBalanceTree::deleteChildTree(Node* pRoot,int elem)
{
	//find elem fail
	if (!pRoot) return m_pRoot;

	//suceess
	if (pRoot->nElem == elem)
	{
		//find root singleton
		if (!pRoot->pLeftChild && !pRoot->pRightChild) pRoot = nullptr;

		else if (!pRoot->pLeftChild) pRoot = pRoot->pRightChild;
		
		else if (!pRoot->pRightChild) pRoot = pRoot->pLeftChild;

		//find minVal from rightTree
		else {
			//using left exchange root  局部指针相当于多一个指针变量，并不会影响原来的指针变量

			Node* rightLeaf = findRightLeaf(pRoot->pLeftChild);
			pRoot->nElem = rightLeaf->nElem;
			pRoot->pLeftChild=deleteChildTree(pRoot->pLeftChild, pRoot->nElem);
			//rightLeaf = rightLeaf->pLeftChild;
			

			//using right exchang root
			/*Node* pLeftLeaf = findLeftLeaf(pRoot->pRightChild);
			pRoot->nElem = pLeftLeaf->nElem;
			pLeftLeaf = pLeftLeaf->pRightChild;*/
		}
	}
	//right child tree
	else if (pRoot->nElem < elem) {
		pRoot->pRightChild = deleteChildTree(pRoot->pRightChild, elem);
	}
	//left child tree
	else {
		pRoot->pLeftChild = deleteChildTree(pRoot->pLeftChild, elem);
	}

	return reBalance(pRoot);

}

TBalanceTree::Node* TBalanceTree::findLeftLeaf(Node* pRightRoot)
{
	return !pRightRoot->pLeftChild ? pRightRoot : findLeftLeaf(pRightRoot->pLeftChild);
}
TBalanceTree::Node* TBalanceTree::findRightLeaf(Node* pLeftRoot)
{
	return !pLeftRoot->pRightChild ? pLeftRoot : findRightLeaf(pLeftRoot->pRightChild);
}


TBalanceTree::Node* TBalanceTree::reBalance(Node* pRoot)
{
	int factor = getFactor(pRoot);

	//LL
	if (factor > 1 && getFactor(pRoot->pLeftChild) > 0)
	{
		return rightRotate(pRoot);
	}
	//LR
	else if (factor > 1 && getFactor(pRoot->pLeftChild) <= 0)
	{
		leftRotate(pRoot->pLeftChild);
		return rightRotate(pRoot);
	}

	//RR
	else if (factor < -1 && getFactor(pRoot->pRightChild) <= 0)
		return leftRotate(pRoot);

	//RL
	else if (factor < -1 && getFactor(pRoot->pRightChild)>0)
	{
		rightRotate(pRoot->pRightChild);
		return leftRotate(pRoot);
	}
	//no change
	else return pRoot;

}


TBalanceTree::Node* TBalanceTree::leftRotate(Node* pRoot)
{
	if (!pRoot)return nullptr;

	//save right
	Node* right = pRoot->pRightChild;
	//rotate
	pRoot->pRightChild = right->pLeftChild;
	right->pLeftChild = pRoot;

	return right;

}

TBalanceTree::Node* TBalanceTree::rightRotate(Node* pRoot)
{
	if (!pRoot)return nullptr;

	//save left
	Node* left = pRoot->pLeftChild;
	//rotate
	pRoot->pLeftChild = left->pRightChild;
	left->pRightChild = pRoot;

	return left;

}


