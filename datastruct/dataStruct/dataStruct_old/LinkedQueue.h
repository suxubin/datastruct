#pragma once

class TLinkedQueue
{
public:
	struct Node{
		Node(int elem=0,Node* nex=nullptr,Node* pre=nullptr){
			nElem=elem;
			pNext=nex;
			pPre=pre;
			
		}
		int nElem;
		Node* pNext;
		Node* pPre;
	};
	TLinkedQueue(int nSize=200);
	~TLinkedQueue();

	bool isEmpty();
	int  getSize();
	void append(int elem);
	int qpop();
	void travel();

private:
	int   m_nMaxSize;
	int	  m_nSize;
	Node* m_pHead;
	Node* m_pRear;


};