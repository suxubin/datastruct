#include <iostream>
#include "LinkedQueue.h"
using namespace std;


TLinkedQueue::TLinkedQueue(int nSize):m_nMaxSize(nSize)
{
	m_nSize=0;
	m_pHead=m_pRear=nullptr;
}

TLinkedQueue::~TLinkedQueue()
{
	delete m_pHead;
	m_pHead=nullptr;
	delete m_pRear;
	m_pRear=nullptr;
}

bool TLinkedQueue::isEmpty()
{
	return m_nSize==0;
}

int TLinkedQueue::getSize()
{
	return m_nSize;
}

void TLinkedQueue::append(int elem)
{
	if(m_nSize>=m_nMaxSize)return;

	if (!m_pHead)
	{
		m_pRear=m_pHead=new Node(elem);
		m_nSize++;
		return;
	}
		
	Node* nex=m_pHead;
	m_pHead=new Node(elem);
	m_pHead->pNext=nex;
	nex->pPre=m_pHead;
	m_nSize++;
	
}

int TLinkedQueue::qpop()
{
	if(isEmpty())return 0;

	int nRes=m_pRear->nElem;
	m_pRear=m_pRear->pPre;
	m_pRear->pNext=nullptr;
	m_nSize--;
	return nRes;
	
}

void TLinkedQueue::travel()
{
	Node* cur=m_pRear;
	while (cur)
	{
		cout<<cur->nElem<<endl;
		cur=cur->pPre;
	}
}