#pragma once

class TArrStack
{
public:
	TArrStack(int nSize=200);
	~TArrStack();

	int spop();
	int speek();
	bool spush(int elem);
	int getSize();
	bool isEmpty();
	void travel();
	
private:
	int* m_pArr;
	int  m_nSize;
	int	 m_nMaxSize;


};