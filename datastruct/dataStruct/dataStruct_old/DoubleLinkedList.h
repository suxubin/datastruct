#pragma once
#include "SingleLinkedList.h"


class TDoubleLinkedList:public TSingleLinkedList
{
public:
	TDoubleLinkedList();
	virtual ~TDoubleLinkedList();

	virtual void add(int elem);
	virtual void append(int elem);
	virtual bool insert(int elem,int pos);
	virtual bool remove(int elem);

};