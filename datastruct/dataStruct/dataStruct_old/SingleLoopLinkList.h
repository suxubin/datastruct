#pragma once
#include "SingleLinkedList.h"

class TSingleLoopLinkList:public TSingleLinkedList
{
public:
	TSingleLoopLinkList();
	virtual ~TSingleLoopLinkList();

	virtual void add(int elem);
	virtual void travel();
	virtual void append(int elem);
	virtual bool insert(int elem,int pos);
	virtual bool remove(int elem);
	virtual bool sreach(int elem);


};