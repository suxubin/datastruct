//红黑树：
//1.节点是红色或者黑色；
//2.根节点是黑色
//3.红色节点的子节点必须是黑色，
//4.从任一节点到树尾端NULL（null可以看作黑色节点）所经过的黑节点树相等

#pragma once
#include "BinaryTree.h"
class RBTree :public TBinaryTree
{
	//typedef bool colorType;

public:
	enum Color {
		Red,
		Black
	};

	//节点关系
	Node* getUncleNode(const Node* curNode);
	Node* getSiblingNode(const Node* curNode);
	Color getColor(const Node* curNode);
	void setColor(Color color);


private:
	Color m_color;
	

};

