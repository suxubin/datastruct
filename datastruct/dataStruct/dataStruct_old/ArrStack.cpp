#include <iostream>
#include "ArrStack.h"
using namespace std;

TArrStack::TArrStack(int nSize):m_nMaxSize(nSize)
{
	m_pArr=new int[m_nMaxSize];
	m_nSize=0;
}

TArrStack::~TArrStack()
{
	delete []m_pArr;
	m_pArr=nullptr;
}

int TArrStack::getSize()
{
	return m_nSize;
}

bool TArrStack::isEmpty()
{
	return m_nSize==0;
}

bool TArrStack::spush(int elem)
{
	if(m_nSize>=m_nMaxSize)return false;

	m_pArr[m_nSize++]=elem;
	return true;
}

int TArrStack::speek()
{
	if(m_nSize<=1)return 0;
	return m_pArr[m_nSize-1];
}

int TArrStack::spop()
{
	int nRes=speek();
	m_nSize--;
	return nRes;
}

void TArrStack::travel()
{
	for (int i=m_nSize-1;i>=0;i--)
		cout<<m_pArr[i]<<endl;

}
