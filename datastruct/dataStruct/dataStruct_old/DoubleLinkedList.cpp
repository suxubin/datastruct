#include <iostream>
#include "DoubleLinkedList.h"
using namespace std;

TDoubleLinkedList::TDoubleLinkedList()
{

}
TDoubleLinkedList::~TDoubleLinkedList()
{

}

void TDoubleLinkedList::add(int elem)
{
	Node* node=new Node(elem);

	if (!m_pHead)
	{
		m_pHead=node;
		m_nSize++;
		return;
	}

	node->pNext=m_pHead;
	m_pHead->pPre=node;
	m_pHead=node;
	m_nSize++;

}

void TDoubleLinkedList::append(int elem)
{
	Node* node=new Node(elem);

	if (!m_pHead)
	{
		m_pHead=node;
		m_nSize++;
		return;
	}

	Node* cur=m_pHead;
	//find endNode
	while (cur->pNext)
		cur=cur->pNext;

	cur->pNext=node;
	node->pPre=cur;
	m_nSize++;

}


bool TDoubleLinkedList::insert(int elem,int pos)
{
	if (pos>m_nSize)return false;
	
	if (pos==0)
	{
		add(elem);
		return true;
	}

	if (pos==m_nSize)
	{
		append(elem);
		return true;
	}

	Node* cur=m_pHead;
	int cnt=1;
	while (cur && pos>cnt)
	{
		cur=cur->pNext;
		cnt++;
	}

	Node* nex=cur->pNext;
	cur->pNext=new Node(elem);
	cur->pNext->pPre=cur;
	nex->pPre=cur->pNext;
	cur->pNext->pNext=nex;
	m_nSize++;
	return true;

}

bool TDoubleLinkedList::remove(int elem)
{
	if(!m_pHead)return false;

	//head node situation
	if (elem==m_pHead->nElem)
	{
		m_pHead=m_pHead->pNext;
		m_pHead->pPre=nullptr;
		return true;
	}

	Node* cur=m_pHead->pNext;
	while (cur)
	{
		if (cur->nElem==elem)
		{
			cur->pPre->pNext=cur->pNext;
			return true;
		}
		cur=cur->pNext;
	}

	return false;
}