#include <iostream>
#include "SingleLoopLinkList.h"
#include "DoubleLinkedList.h"
#include "ArrStack.h"
#include "LinkedQueue.h"
#include "BinaryTree.h"
#include "balanceTree.h"
using namespace std;

int main()
{
	//TSingleLinkedList* baseLinkList=new TSingleLinkedList();
	//baseLinkList->append(10);
	//baseLinkList->append(100);
	//baseLinkList->append(1000);
	//baseLinkList->insert(30,3);
	//baseLinkList->travel();
	//delete baseLinkList;
	//baseLinkList=nullptr;

	//TSingleLinkedList* linkList=new TSingleLoopLinkList();
	//linkList->add(10);
	//linkList->add(100);
	//linkList->add(1000);
	//linkList->append(20);
	//linkList->append(200);
	//linkList->append(2000);
	//linkList->insert(3000,4);
	//linkList->remove(200);
	//linkList->travel();
	//cout<<linkList->sreach(2000)<<endl;
	//delete linkList;
	//linkList=nullptr;

	//TSingleLinkedList* DLinklist=new TDoubleLinkedList();
	//DLinklist->add(30);
	//DLinklist->add(300);
	//DLinklist->append(40);
	//DLinklist->insert(400,1);
	//DLinklist->remove(400);
	//DLinklist->travel();
	//cout<<DLinklist->sreach(40)<<endl;
	//delete DLinklist;
	//DLinklist=nullptr;

	//TArrStack arrStack;
	//arrStack.spush(20);
	//arrStack.spush(30);
	//arrStack.spush(40);
	//arrStack.spop();
	//arrStack.travel();

	//TLinkedQueue linkedQue;
	//linkedQue.append(1);
	//linkedQue.append(2);
	//linkedQue.append(3);
	//linkedQue.append(4);
	//linkedQue.qpop();
	//linkedQue.travel();

	TBinaryTree bTree;
	bTree.add(0);
	bTree.add(1);
	bTree.add(2);
	bTree.add(3);
	bTree.add(4);
	bTree.add(5);
	bTree.add(6);
	bTree.add(7);
	bTree.add(8);
	bTree.add(9);
	cout << bTree.getTreeHeight() << endl;
	cout << bTree.isBalanceTree() << endl;
	bTree.breadchTravel();
	bTree.preOrder(bTree.getRoot());cout<<endl;
	bTree.midOrder(bTree.getRoot());cout<<endl;
	bTree.lastOrder(bTree.getRoot());cout<<endl;

	//balance tree
	/*TBalanceTree temp;
	temp.insert(1);
	temp.insert(2);
	temp.insert(3);
	temp.insert(4);
	temp.insert(5);
	temp.insert(6);
	temp.deleteNode(2);
	temp.breadchTravel();*/

	system("pause");
	return 0;

}