#include <iostream>
#include "SingleLoopLinkList.h"
using namespace std;


TSingleLoopLinkList::TSingleLoopLinkList()
{

}

TSingleLoopLinkList::~TSingleLoopLinkList()
{

}

void TSingleLoopLinkList::add(int elem)
{
	Node* node=new Node(elem);
	if (!m_pHead)
	{
		m_pHead=node;
		node->pNext=m_pHead;
		m_nSize++;
		return;
	}

	Node* cur=m_pHead;
	while(cur->pNext!=m_pHead)
		cur=cur->pNext;

	Node* nex=m_pHead;
	m_pHead=node;
	m_pHead->pNext=nex;
	cur->pNext=m_pHead;
	m_nSize++;

}

void TSingleLoopLinkList::append(int elem)
{
	Node* node=new Node(elem);
	if (!m_pHead)
	{
		m_pHead=node;
		node->pNext=m_pHead;
		m_nSize++;
		return;
	}

	Node* cur=m_pHead;
	while (cur->pNext!=m_pHead)
		cur=cur->pNext;
	
	cur->pNext=node;
	cur->pNext->pNext=m_pHead;
	m_nSize++;

}

bool TSingleLoopLinkList::insert(int elem,int pos)
{
	Node* node=new Node(elem);
	if (m_nSize<pos)return false;

	if (pos==0)
	{
		add(elem);
		return true;
	}

	//not nullptr situation
	Node* cur=m_pHead;
	int cnt=1;
	while(cur->pNext!=m_pHead && pos>cnt)
	{
		cur=cur->pNext;
		cnt++;
	}

	//end node
	if (cur->pNext==m_pHead)
	{
		cur->pNext=node;
		cur->pNext->pNext=m_pHead;
		m_nSize++;
		return true;
	}

	Node* nex=cur->pNext;
	cur->pNext=node;
	cur->pNext->pNext=nex;
	m_nSize++;
	return true;

}

bool TSingleLoopLinkList::remove(int elem)
{
	if(!m_pHead)return false;

	Node* cur=m_pHead;
	Node* pre=m_pHead;

	while (cur->pNext!=m_pHead)
	{	
		if (cur->nElem==elem)
		{
			//head node situation
			if (cur==m_pHead)
			{ //find end node
				Node* rear=m_pHead;
				while(rear!=m_pHead)
					rear=rear->pNext;
				m_pHead=cur->pNext;
				rear->pNext=m_pHead;
				return true;
			}//if
			pre->pNext=cur->pNext;
			return true;
		}//if
		pre=cur;
		cur=cur->pNext;
	}//while

	//end node 
	if (cur->pNext==m_pHead)
	{
		pre->pNext=cur->pNext;
		return true;
	}

	return false;
}

bool TSingleLoopLinkList::sreach(int elem)
{
	Node* cur=m_pHead;
	while (cur->pNext!=m_pHead)
	{
		if (cur->nElem==elem)return true;
		cur=cur->pNext;
	}
	if(cur->nElem==elem)return true;
	return false;
}

void TSingleLoopLinkList::travel()
{
	if (!m_pHead)return;

	Node* cur=m_pHead;
	while (cur->pNext!=m_pHead)
	{
		cout<<cur->nElem<<endl;
		cur=cur->pNext;
	}cout<<cur->nElem<<endl;
		
	
}
