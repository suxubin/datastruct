#include <iostream>
#include "SingleLinkedList.h"
using namespace std;


TSingleLinkedList::TSingleLinkedList(Node* head,int nSize)
	:m_pHead(head),m_nSize(nSize)
{

}
TSingleLinkedList::~TSingleLinkedList()
{
	if(m_pHead!=nullptr)
	{
		delete m_pHead;
		m_pHead=nullptr;
	}
}

bool TSingleLinkedList::isEmpty()
{
	return m_nSize==0;
}

int TSingleLinkedList::length()
{
	return m_nSize;
}

void TSingleLinkedList::append(int elem)
{
	if (isEmpty())
	{
		m_pHead=new Node(elem);
		m_nSize++;
		return ;
	}

	Node* cur=m_pHead;
	while (cur->pNext)
		cur=cur->pNext;

	cur->pNext=new Node(elem);
	m_nSize++;
	
}

void TSingleLinkedList::add(int elem)
{
	Node* head=new Node(elem);
	head->pNext=m_pHead;
	m_pHead=head;
	m_nSize++;
}

bool TSingleLinkedList::insert(int elem,int pos)
{
	if(m_nSize<pos)return false;

	if(pos==0)
	{
		add(elem);
		return true;
	}

	Node* cur=m_pHead;
	int cnt=1;
	while (cur && pos>cnt)
	{
		cnt++;
		cur=cur->pNext;
	}

	Node* nex=cur->pNext;
	cur->pNext=new Node(elem);
	cur->pNext->pNext=nex;
	m_nSize++;
	return true;
	
}

bool TSingleLinkedList::remove(int elem)
{
	if(!m_pHead)return false;

	if (m_pHead->nElem==elem)
	{
		m_pHead=m_pHead->pNext;
		m_nSize--;
		return true;
	}

	Node* cur =m_pHead->pNext;
	Node* pre=m_pHead;
	while (cur)
	{
		if (cur->nElem==elem)
		{
			pre->pNext=cur->pNext;
			m_nSize--;
			return true;
		}
		pre=cur;
		cur=cur->pNext;
	}

	return false;
}

bool TSingleLinkedList::sreach(int elem)
{
	Node* cur=m_pHead;

	while (cur)
	{
		if(cur->nElem==elem)return true;
		cur=cur->pNext;
	}
	return false;
}

void TSingleLinkedList::travel()
{
	Node* cur=m_pHead;

	while (cur)
	{
		cout<<cur->nElem<<endl;
		cur=cur->pNext;
	}

}

